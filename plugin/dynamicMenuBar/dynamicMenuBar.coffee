DynamicMenuBar = (data) ->

	extend = (a, b) ->
		for i of b
			a[i] = b[i]

	includeLinkCss = ->
		menuBarCss = document.createElement('link')
		menuBarCss.setAttribute 'rel', 'stylesheet'
		menuBarCss.setAttribute 'href', 'plugin/dynamicMenuBar/dynamicMenuBar_auto.css'
		document.head.appendChild menuBarCss

	insertHtmlTree = (tree) ->
		navBar = document.createElement('nav')
		navBar.innerHTML = tree
		document.body.appendChild navBar

	convertTreeToHtml = (data) ->
		htmlMenuTree = '<ul>'

		recursiveHtmlBuilder = (data) ->
			i = 0
			while i < data.length
				htmlMenuTree = htmlMenuTree + '<li treeIndex="' + data[i].treeIndex + '"><a href="#/' + data[i].slideIndex + '">' + data[i].title + '</a>'
				if data[i].children.length > 0
					htmlMenuTree = htmlMenuTree + '<ul>'
					recursiveHtmlBuilder data[i].children
					htmlMenuTree = htmlMenuTree + '</ul>'
				htmlMenuTree = htmlMenuTree + '</li>'
				i++

		recursiveHtmlBuilder data
		htmlMenuTree = htmlMenuTree + '</ul>'
		htmlMenuTree

	buildMenuTree = (analyser) ->
		iterable = analyser.getIterable(document)
		index = 0
		structuredIndex = []
		initialLevel = analyser.analyse(iterable[index]).level

		recursiveBuilder = (level) ->
			tree = []
			while index < iterable.length
				children = []
				if !structuredIndex[level - 1]
					structuredIndex[level - 1] = 0
				structuredIndex[level - 1]++
				iterable[index].parentNode.setAttribute 'treeIndex', structuredIndex.join('.')
				menuData = analyser.analyse(iterable[index])
				menuNextLevel = 0
				if index + 1 < iterable.length
					menuNextLevel = analyser.analyse(iterable[index + 1]).level
				if menuNextLevel > level
					index++
					children = recursiveBuilder(menuNextLevel)
				tree.push
					'title': menuData['title']
					'treeIndex': menuData['treeIndex']
					'slideIndex': menuData['slideIndex']
					'children': children
				if index + 1 < iterable.length
					menuNextLevel = analyser.analyse(iterable[index + 1]).level
				if menuNextLevel < level
					structuredIndex.pop()
					break
				index++
			tree

		recursiveBuilder initialLevel

	HtmlAnalyser = ->

		@getIterable = (data) ->
			data.querySelectorAll 'h1,h2,h3,h4,h5,h6'

		@analyse = (data) ->
			{
			'level': data.tagName.substring(1)
			'title': data.getAttribute('menu') || data.innerHTML
			'treeIndex': data.parentNode.getAttribute('treeIndex')
			'slideIndex': data.parentNode.getAttribute('slideIndex')
			}
		return @

	addTreeIndexForNoTitleSlide = ->
		slides = document.querySelectorAll '[slideindex]'
		treeIndex = 0
		i = 0
		while i < slides.length
			slide = slides[i]
			if(slide.getAttribute('treeIndex'))
				treeIndex = slide.getAttribute('treeIndex')
			else
				slide.setAttribute('treeIndex', treeIndex)
			i++


	DataMenuOnlyAnalyser = ->
#this.sectionList = document.querySelectorAll('.slide>section');


	addSectionTagIndex = ->
		hSectionList = document.querySelectorAll('.slides>section')
		i = 0
		while i < hSectionList.length
			hSectionList[i].setAttribute 'slideIndex', i
			vSectionList = hSectionList[i].querySelectorAll('section')
			j = 0
			while j < vSectionList.length
				vSectionList[j].setAttribute 'slideIndex', i + '/' + j
				j++
			i++

	resetMenuPosition = ->
		resetList = document.querySelectorAll('nav li.present, nav li.past')
		i = 0
		while i < resetList.length
			presentLi = resetList[i]
			presentLi.className = ''
			i++

	tagMenuPresent = (currentSlide) ->
		currentIndexParts = currentSlide.getAttribute('treeIndex').split('.')
		while currentIndexParts.length
			selector = 'li[treeIndex="' + currentIndexParts.join('.') + '"]'
			selected =document.querySelector(selector)
			selected.className = 'present' if selected
			currentIndexParts.pop()

	tagMenuPast = () ->
		presentsParts = document.querySelectorAll('nav .present')
		stepBefore = (eList) ->
			newList = []
			for key, elem of eList
				newList.push elem.previousElementSibling if elem.previousElementSibling && elem.previousElementSibling.tagName == 'LI'
			return newList
		tagableParts = stepBefore presentsParts
		while tagableParts.length
			for i, e of tagableParts
				e.className = 'past'
			tagableParts = stepBefore tagableParts

	resetCurrentEtape = () ->
		resetList = document.querySelectorAll('a[class^="etape"]')
		i = 0
		while i < resetList.length
			presentLi = resetList[i]
			presentLi.className = ''
			i++

	getNextCategorieSlideNumber = (elem) ->
		if(elem.nextElementSibling)
			return elem.nextElementSibling.firstElementChild.getAttribute('href').split('/')[1]
		else
			treeFragment = elem.getAttribute('treeIndex').split('.')
			while treeFragment.length
				treeFragment.pop()
				treeFragment[treeFragment.length-1] = parseInt(treeFragment[treeFragment.length-1])+1
				nextTreeIndex = treeFragment.join('.')
				nextElement = document.querySelector 'li[treeIndex="'+nextTreeIndex+'"]'
				if(nextElement)
					return getPresentCategorieSlideNumber (nextElement)
			slides = document.querySelectorAll('[slideIndex]')
			return slides[slides.length-1].getAttribute('slideIndex')


	getPresentCategorieSlideNumber = (elem)->
		elem.firstElementChild.getAttribute('href').split('/')[1]

	showCurrentEtape = (currentSlide) ->
		calculateClassEtape = (currentSlide, target) ->
			nextCategorieSlideNumber = getNextCategorieSlideNumber(target)
			presentCategorieSlideNumber = getPresentCategorieSlideNumber(target)
			currentSlideNumber = currentSlide.getAttribute('slideindex')
			total =  nextCategorieSlideNumber - presentCategorieSlideNumber
			current = currentSlideNumber - presentCategorieSlideNumber
			return 'etape'+current+'sur'+total
		presentList = document.querySelectorAll('nav li.present')
		for elem in presentList
			console.log(elem.firstElementChild.innerText, elem.firstElementChild.getAttribute('href').split('/')[1])
			elem.firstElementChild.classList.add(calculateClassEtape(currentSlide, elem))
			console.log(calculateClassEtape(currentSlide, elem))


	updateMenuProgress = (eventData) ->
		currentSlide = document.querySelector('.slides>section.present[treeIndex], section.stack.present section.present[treeIndex]')
		if currentSlide
			resetMenuPosition()
			tagMenuPresent currentSlide
			tagMenuPast()
			resetCurrentEtape()
			showCurrentEtape currentSlide

	updateOnEvents = (eventTabList) ->
		revealEventTransmitter = document.querySelector('.reveal')
		for i of eventTabList
			revealEventTransmitter.addEventListener eventTabList[i], updateMenuProgress


	addSectionTagIndex()
	includeLinkCss()
	menuTree = buildMenuTree(new HtmlAnalyser)
	addTreeIndexForNoTitleSlide()
	htmlMenuTree = convertTreeToHtml(menuTree)
	# génère l'arbre html
	insertHtmlTree htmlMenuTree
	# insère l'arbre dans la page
	#	apa.addMetrics(calculateTreePart);
	updateOnEvents [ 'cmhChanged' ]

# ---
# generated by js2coffee 2.0.3
