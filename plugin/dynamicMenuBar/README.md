Il y a plusieurs façons d'identifier la hiérarchie des parties de la présentation. De la plus implicite à la plus explicite et verbeuse :
Dans aucun cas on ne tient compte des slides verticales qui sont considérées comme optionnelles.
- les balises h1, h2, h3... de chaque slide désignent leur niveau et donc la partie à laquelle appartiennent les slides suivantes de niveau inferieur.
- un titre court (pour afficher dans le menu) peut être défini :
  + en mettant un span autour de la partie du titre long qui correspond au titre court (toujours dans la balise h1 ou hn de la slide)
  + en mettant un attribut title, alt ou data-short à la balise h1 ou hn du slide.
- l'attribut data-menu peut être ajouté à la balise section pour indiquer le niveau et le titre court de façon prioritaire au hn contenu dans la slide ou si la slide ne contient pas de titre.
- vous pouvez également utiliser data-menu comme moyen exclusif de définir la hiérarchie des parties pour qu'aucun h1 ou hn ne soit pris en compte. Dans ce cas les sections sans data-menu seront considérées comme filles de la dernière slide avec data-menu trouvée. Pour activer ce mode de fonctionnement, ajoutez la classe dataMenuOnly à la balise div.slides.

Syntaxe du contenu des data-menu="hn>titre court>titre long" avec hn obligatoire (h1, h2, h3, h4, h5, h6) puis de façon facultative sauf en mode dataMenuOnly où cela devient obligatoire : > et le titre court qui appraîtra dans le menu, éventuellement suivi d'un titre long si l'on ne veux pas du tout se servir des balises h1 et hn.



