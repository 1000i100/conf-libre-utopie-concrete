describe('Custom Metric Handler', function() {
  return it('Il peut fusionner deux objets.', function() {
    var base, bonux;
    base = {
      toto: 'toto'
    };
    bonux = {
      tata: 'tata'
    };
    extend(base, bonux);
    return base.should.eql({
      toto: 'toto',
      tata: 'tata'
    });
  });
});
