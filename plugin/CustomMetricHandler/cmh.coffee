#Récupère les métriques des pages (totales,  n°en cours etc...)
do ->
	revealEventTransmitter = document.querySelector('.reveal')
	extend = (a, b) ->
		for i of b
			a[i] = b[i]


	dispatchEvent = (type, properties) ->
		event = document.createEvent('HTMLEvents', 1, 2)
		event.initEvent type, true, true
		extend event, properties
		revealEventTransmitter.dispatchEvent event

	updatePageNumber = (eventData) ->
		for i of customMetricsGenerators
			extend cmh, customMetricsGenerators[i](eventData)
		dispatchEvent 'cmhChanged', cmh
		return

	updateOnEvents = (eventTabList) ->
		for i of eventTabList
			Reveal.addEventListener eventTabList[i], updatePageNumber


	getHSlideList = ->
		if !getHSlideList.res
			getHSlideList.res = document.querySelectorAll('.reveal .slides>section')
		getHSlideList.res

	getVSlideList = ->
		if !getVSlideList.res
			getVSlideList.res = document.querySelectorAll('.slides>section>section')
		getVSlideList.res

	calculateGlobalHSlide = (eventData) ->
		totalHSlide = getHSlideList().length
		currentHSlide = 1
		if cmh.currentHSlide
			currentHSlide = cmh.currentHSlide
		if eventData.indexh != undefined
			currentHSlide = eventData.indexh + 1
		{
		'currentHSlide': currentHSlide
		'totalHSlide': totalHSlide
		'currentGlobalHSlide': currentHSlide
		'totalGlobalHSlide': totalHSlide
		}

	calculateGlobalVSlide = (eventData) ->
		if calculateGlobalVSlide.nbrHSlideWhithVSlides == undefined
			calculateGlobalVSlide.nbrHSlideWhithVSlides = document.querySelectorAll('.slides>section>section:first-child').length
		totalGlobalVSlide = getVSlideList().length - calculateGlobalVSlide.nbrHSlideWhithVSlides
		currentGlobalVSlide = document.querySelectorAll('.slides>section.stack.present>section.present, .slides>section.stack.present>section.past, .slides>section.stack.past>section').length - document.querySelectorAll('.slides>section.stack.present, .slides>section.stack.past').length
		{
		'totalGlobalVSlide': totalGlobalVSlide
		'currentGlobalVSlide': currentGlobalVSlide
		}

	calculateLocalVSlide = (eventData) ->
		`var currentLocalVSlide`
		currentHSlide = getHSlideList()[cmh.currentHSlide - 1]
		localVSlideList = currentHSlide.querySelectorAll('section')
		totalLocalVSlide = localVSlideList.length
		currentLocalVSlide = 0
		if cmh.currentLocalVSlide
			currentLocalVSlide = cmh.currentLocalVSlide
		if eventData.indexv != undefined
			currentLocalVSlide = eventData.indexv
		{
		'currentLocalVSlide': currentLocalVSlide
		'totalLocalVSlide': totalLocalVSlide
		}

	calculateGlobalSlide = ->
		{
		'currentGlobalSlide': cmh.currentGlobalHSlide + cmh.currentGlobalVSlide
		'totalGlobalSlide': cmh.totalGlobalHSlide + cmh.totalGlobalVSlide
		}

	calculateGlobalStep = ->
		{
		'currentGlobalStep': cmh.currentGlobalHSlide + cmh.currentGlobalVSlide + cmh.currentGlobalFragment
		'totalGlobalStep': cmh.totalGlobalHSlide + cmh.totalGlobalVSlide + cmh.totalGlobalFragment
		}

	#calculateGlobalLinearStep (sans les VSlide et les fragment de VSlide)

	calculateLocalFragment = ->
		currentSlide = undefined
		currentHSlide = getHSlideList()[cmh.currentHSlide - 1]
		localVSlideList = currentHSlide.querySelectorAll('section')
		if localVSlideList.length > 0
			currentSlide = localVSlideList[cmh.currentLocalVSlide]
		else
			currentSlide = currentHSlide
		totalLocalFragment = currentSlide.querySelectorAll('.fragment').length
		hasFragments = totalLocalFragment > 0
		if hasFragments
			visibleFragments = currentSlide.querySelectorAll('.fragment.visible')
			return {
			'currentLocalFragment': visibleFragments.length
			'totalLocalFragment': totalLocalFragment
			}
		{
		'currentLocalFragment': 0
		'totalLocalFragment': 0
		}

	calculateGlobalFragment = ->
		if calculateGlobalFragment.totalGlobalFragment == undefined
			calculateGlobalFragment.totalGlobalFragment = document.querySelectorAll('.slides .fragment').length
		currentGlobalFragment = document.querySelectorAll('.slides .fragment.visible').length
		{
		'currentGlobalFragment': currentGlobalFragment
		'totalGlobalFragment': calculateGlobalFragment.totalGlobalFragment
		}

	window.cmh = {}
	cmh.events = [
		'ready'
		'slidechanged'
		'fragmentshown'
		'fragmenthidden'
	]
	customMetricsGenerators = []

	cmh.addMetrics = (triggerFunction) ->
# triggerFunction must return a json result {customMetric1:value1,customMetric2:value2...}
		customMetricsGenerators.push triggerFunction

	cmh.addMetrics calculateGlobalHSlide
	cmh.addMetrics calculateGlobalVSlide
	cmh.addMetrics calculateGlobalFragment
	cmh.addMetrics calculateGlobalSlide
	cmh.addMetrics calculateGlobalStep
	cmh.addMetrics calculateLocalVSlide
	cmh.addMetrics calculateLocalFragment
	updateOnEvents cmh.events
