do ->
	rgbToHex= (r, g, b)->
		return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
	LightenDarkenColor = (col, amt) ->
		usePound = false
		if col[0] == '#'
			col = col.slice(1)
			usePound = true
		num = parseInt(col, 16)
		r = (num >> 16) + amt
		if r > 255
			r = 255
		else if r < 0
			r = 0
		b = (num >> 8 & 0x00FF) + amt
		if b > 255
			b = 255
		else if b < 0
			b = 0
		g = (num & 0x0000FF) + amt
		if g > 255
			g = 255
		else if g < 0
			g = 0
		(if usePound then '#' else '') + (g | b << 8 | r << 16).toString(16)

	updateBackground = (eventData) ->
		###
  		récupérer la couleur de l'élément actuel du menu, le plus profond dans l'arboresence
  		appliquer cette couleur en fond de slide.

  		puis ajuster en ligten 20 et 40 pour en faire un dégradé radial plutot qu'un applat
		###
		currentStep = document.querySelector('li.present a')
		stepRgbColor = window.getComputedStyle(currentStep).borderBottomColor.split('(')[1].split(')')[0].split(',')
		stepColor = rgbToHex(parseInt(stepRgbColor[0]),parseInt(stepRgbColor[1]),parseInt(stepRgbColor[2]));
		centerColor = "rgba(100%,100%,100%,0)"
		#borderColor = LightenDarkenColor(stepColor, 60)
		borderColor = 'rgba('+stepRgbColor[0]+','+stepRgbColor[1]+','+stepRgbColor[2]+',0.3)'
		document.getElementsByTagName("body")[0].style.background =
			"radial-gradient(ellipse farthest-corner at center, "+centerColor+", "+borderColor+")"
#	background: radial-gradient(ellipse farthest-corner at center, white, #ffb380)


	updateOnEvents = (eventTabList) ->
		revealEventTransmitter = document.querySelector('.reveal')
		for i of eventTabList
			revealEventTransmitter.addEventListener eventTabList[i], updateBackground

	updateOnEvents [ 'cmhChanged' ]
